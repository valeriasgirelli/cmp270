/*
 * Copyright (C) 2009~2015  Marco Antonio Zanata Alves
 *                          (mazalves at inf.ufrgs.br)
 *                          GPPD - Parallel and Distributed Processing Group
 *                          Universidade Federal do Rio Grande do Sul
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../sinuca.hpp"

#ifdef BRANCH_PREDICTOR_DEBUG
    #define BRANCH_PREDICTOR_DEBUG_PRINTF(...) DEBUG_PRINTF(__VA_ARGS__);
#else
    #define BRANCH_PREDICTOR_DEBUG_PRINTF(...)
#endif

// ============================================================================
branch_predictor_static_taken_t::branch_predictor_static_taken_t() {
    this->branch_predictor_type = BRANCH_PREDICTOR_STATIC_TAKEN;

    this->btb = NULL;
    this->btb_line_number = 0;
    this->btb_associativity = 0;
    this->btb_total_sets = 0;
    this->btb_replacement_policy = REPLACEMENT_LRU;

    this->return_address_stack_pos = 0;
    this->return_address_stack = NULL;

    this->indirect_target_buffer_size = 0;
    this->indirect_target_buffer = NULL;
    this->indirect_target_buffer_bits_mask = 0;
    this->branch_history_register = 0;

    this->btb_index_bits_shift = 0;
    this->btb_index_bits_mask = 0;
    this->btb_tag_bits_shift = 0;
    this->btb_tag_bits_mask = 0;
};

// ============================================================================
branch_predictor_static_taken_t::~branch_predictor_static_taken_t() {
    /// De-Allocate memory to prevent memory leak
    utils_t::template_delete_array<branch_target_buffer_set_t>(btb);
    utils_t::template_delete_array<uint64_t>(return_address_stack);
    utils_t::template_delete_array<uint64_t>(indirect_target_buffer);

};

// ============================================================================
void branch_predictor_static_taken_t::allocate() {
    branch_predictor_t::allocate();

    ERROR_ASSERT_PRINTF(utils_t::check_if_power_of_two(this->get_btb_line_number() / this->get_btb_associativity()), "Wrong line number(%u) or associativity(%u).\n", this->get_btb_line_number(), this->get_btb_associativity());
    ERROR_ASSERT_PRINTF(utils_t::check_if_power_of_two(this->get_indirect_target_buffer_size()), "Wrong Indirect Taget Buffer size (%u).\n", this->get_indirect_target_buffer_size());

    uint32_t i;

    this->set_btb_total_sets(this->get_btb_line_number() / this->get_btb_associativity());

    this->btb = utils_t::template_allocate_array<branch_target_buffer_set_t>(this->get_btb_total_sets());
    for (i = 0; i < this->get_btb_total_sets(); i++) {
        this->btb[i].ways = utils_t::template_allocate_array<branch_target_buffer_line_t>(this->get_btb_associativity());
    }

    this->return_address_stack = utils_t::template_allocate_initialize_array<uint64_t>(this->get_return_address_stack_size(), 0);
    this->indirect_target_buffer = utils_t::template_allocate_initialize_array<uint64_t>(this->get_indirect_target_buffer_size(), 0);

    /// BTB INDEX MASK
    this->btb_index_bits_shift = 0;
    for (i = 0; i < utils_t::get_power_of_two(this->get_btb_total_sets()); i++) {
        this->btb_index_bits_mask |= 1 << (i + btb_index_bits_shift);
    }

    /// BTB TAG MASK
    this->btb_tag_bits_shift = btb_index_bits_shift + utils_t::get_power_of_two(this->get_btb_total_sets());
    for (i = btb_tag_bits_shift; i < utils_t::get_power_of_two((uint64_t)INT64_MAX+1); i++) {
        this->btb_tag_bits_mask |= 1 << i;
    }
    ERROR_ASSERT_PRINTF(btb_get_index((uint64_t)INT64_MAX+1) < this->get_btb_total_sets(), "Index cannot be bigger than btb_total_sets \n")

    /// Indirect Target Branch MASK
    for (i = 0; i < utils_t::get_power_of_two(this->get_indirect_target_buffer_size()); i++) {
        this->indirect_target_buffer_bits_mask |= 1 << i;
    }
};

// ============================================================================
uint32_t branch_predictor_static_taken_t::btb_evict_address(uint64_t opcode_address) {
    uint64_t index = btb_get_index(opcode_address >> 2);
    uint32_t way = 0;
    uint32_t selected = 0;

    switch (this->btb_replacement_policy) {
        case REPLACEMENT_LRU: {
            uint64_t last_access = UINT64_MAX;
            for (way = 0; way < this->get_btb_associativity(); way++) {
                /// If there is free space
                if (this->btb[index].ways[way].last_access <= last_access) {
                    selected = way;
                    last_access = this->btb[index].ways[way].last_access;
                }
            }
            break;
        }
        case REPLACEMENT_RANDOM: {
            // initialize random seed
            unsigned int seed = time(NULL);
            // generate random number
            selected = (rand_r(&seed) % this->get_btb_associativity());
        }
        break;

        case REPLACEMENT_INVALID_OR_LRU:
            ERROR_PRINTF("Replacement Policy: REPLACEMENT_INVALID_OR_LRU not implemented.\n");
        break;

        case REPLACEMENT_FIFO:
            ERROR_PRINTF("Replacement Policy: REPLACEMENT_POLICY_FIFO not implemented.\n");
        break;

        case REPLACEMENT_LRF:
            ERROR_PRINTF("Replacement Policy: REPLACEMENT_POLICY_LRF not implemented.\n");
        break;

        case REPLACEMENT_DEAD_OR_LRU:
            ERROR_PRINTF("Replacement Policy: REPLACEMENT_DEAD_OR_LRU should not use for branch_prediction.\n");
        break;

        case REPLACEMENT_SRRIP:
            ERROR_PRINTF("Replacement Policy: REPLACEMENT_SRRIP should not use for branch_prediction.\n");
        break;
    }

    return selected;
};


// ============================================================================
uint64_t branch_predictor_static_taken_t::btb_find_update_address(const opcode_package_t& actual_opcode, uint64_t next_opcode_address) {
    uint64_t index = btb_get_index(actual_opcode.opcode_address >> 2);
    uint64_t tag = btb_get_tag(actual_opcode.opcode_address >> 2);
    uint32_t way = 0;

    this->add_stat_btb_accesses();

    ERROR_ASSERT_PRINTF(index < this->get_btb_total_sets(), "Index >= btb_total_sets \n")
    for (way = 0 ; way < this->get_btb_associativity() ; way++) {
        /// BTB HIT
        if (this->btb[index].ways[way].tag == tag) {
            uint64_t previous_address = this->btb[index].ways[way].target_address;

            this->btb[index].ways[way].target_address = next_opcode_address;
            this->btb[index].ways[way].last_access = sinuca_engine.get_global_cycle();
            this->btb[index].ways[way].usage_counter++;
            this->add_stat_btb_hit();
            return previous_address;
        }
    }

    /// BTB MISS
    way = this->btb_evict_address(actual_opcode.opcode_address);
    this->btb[index].ways[way].tag = tag;
    this->btb[index].ways[way].target_address = next_opcode_address;
    this->btb[index].ways[way].last_access = sinuca_engine.get_global_cycle();
    this->btb[index].ways[way].usage_counter = 1;
    this->add_stat_btb_miss();
    return 0;
};

// ============================================================================
processor_stage_t branch_predictor_static_taken_t::predict_branch(const opcode_package_t& actual_opcode, uint64_t next_opcode_address) {
    processor_stage_t solve_stage = PROCESSOR_STAGE_FETCH;

    if (actual_opcode.opcode_operation != INSTRUCTION_OPERATION_BRANCH) {
        solve_stage = PROCESSOR_STAGE_FETCH;
    }
    else {
        this->add_stat_branch_predictor_operation();

        bool predicted_direction_is_taken = true;
        uint64_t predicted_target_address = 0;

        uint64_t next_sequential_address = actual_opcode.opcode_address + actual_opcode.opcode_size;
        bool next_real_direction_is_taken = (next_sequential_address != next_opcode_address);


        /// Checks for all jumps (cond/uncond)
        predicted_target_address = this->btb_find_update_address(actual_opcode, next_opcode_address);

        switch (actual_opcode.branch_type) {
            // Conditional, 2 targets, Solve on Execution
            case BRANCH_COND:
                add_stat_branch_predictor_conditional();
                if (next_real_direction_is_taken) {
                    add_stat_branch_predictor_conditional_taken();
                }
                else {
                    add_stat_branch_predictor_conditional_not_taken();
                }

                /// Update the branch history for iBTB
                this->branch_history_register <<= 1;                           /// Make room for the next branch
                this->branch_history_register |= next_real_direction_is_taken; /// Update the signature

                // Direction Miss Prediction
                if (predicted_direction_is_taken != next_real_direction_is_taken) {
                    add_stat_branch_predictor_conditional_miss();
                    solve_stage = PROCESSOR_STAGE_EXECUTION;
                }
                // Direction Correct Prediction
                // If Target Miss Prediction
                else if(predicted_target_address == 0) {
                    add_stat_branch_predictor_conditional_address_miss();
                    solve_stage = PROCESSOR_STAGE_DECODE;
                }
            break;

            // Syscall, 1 target, Solve on Fetch
            case BRANCH_SYSCALL:
                add_stat_branch_predictor_syscall();
                solve_stage = PROCESSOR_STAGE_EXECUTION;    // Add some extra latency (we do not trace syscalls)
            break;

            // Unconditional: Direct, 1 target, Solve on Decode
            // Unconditional: Indirect, N targets, Solve on Execution
            case BRANCH_UNCOND:
                if (actual_opcode.is_indirect) {
                    add_stat_branch_predictor_indirect();
                    /// Obtain/Update the indirect target buffer data
                    uint64_t ibtb_index = indirect_target_buffer_get_index(this->branch_history_register, actual_opcode.opcode_address);
                    predicted_target_address = this->indirect_target_buffer[ibtb_index];
                    this->indirect_target_buffer[ibtb_index] = next_opcode_address;
                    if (predicted_target_address != next_opcode_address) {
                        add_stat_branch_predictor_indirect_miss();
                        solve_stage = PROCESSOR_STAGE_EXECUTION;
                    }
                }
                else {
                    add_stat_branch_predictor_unconditional();
                    if (predicted_target_address != next_opcode_address) {
                        add_stat_branch_predictor_unconditional_miss();
                        solve_stage = PROCESSOR_STAGE_DECODE;
                    }
                }
            break;

            // Call: Direct, 1 target, Solve on Decode *OR* Indirect, N targets, Solve on Execution
            case BRANCH_CALL:
                if (actual_opcode.is_indirect) {
                    add_stat_branch_predictor_indirect();
                    /// Obtain/Update the indirect target buffer data
                    uint64_t ibtb_index = indirect_target_buffer_get_index(this->branch_history_register, actual_opcode.opcode_address);
                    predicted_target_address = this->indirect_target_buffer[ibtb_index];
                    this->indirect_target_buffer[ibtb_index] = next_opcode_address;
                    if (predicted_target_address != next_opcode_address) {
                        add_stat_branch_predictor_indirect_miss();
                        solve_stage = PROCESSOR_STAGE_EXECUTION;
                    }
                }
                else {
                    add_stat_branch_predictor_call();
                    if (predicted_target_address != next_opcode_address) {
                        add_stat_branch_predictor_call_miss();
                        solve_stage = PROCESSOR_STAGE_DECODE;
                    }
                }
                // Push to the Return Address Stack
                if (this->return_address_stack_pos < this->return_address_stack_size) {
                    this->return_address_stack[this->return_address_stack_pos] = next_sequential_address;
                    this->return_address_stack_pos++;
                }
            break;

            // Return: Indirect, N targets, Solve on Execution
            case BRANCH_RETURN:
                add_stat_branch_predictor_return();
                // Pop and Use the Return Address Stack
                if (this->return_address_stack_pos > 0) {
                    this->return_address_stack_pos--;
                    predicted_target_address = this->return_address_stack[this->return_address_stack_pos];
                    this->return_address_stack[this->return_address_stack_pos] = 0;
                }
                if (predicted_target_address != next_opcode_address) {
                    add_stat_branch_predictor_return_miss();
                    solve_stage = PROCESSOR_STAGE_EXECUTION;
                }
            break;
        };
    }


    return solve_stage;
};

// ============================================================================
void branch_predictor_static_taken_t::print_structures() {
    branch_predictor_t::print_structures();
};

// ============================================================================
void branch_predictor_static_taken_t::panic() {
    branch_predictor_t::panic();

    this->print_structures();
};

// ============================================================================
void branch_predictor_static_taken_t::periodic_check(){
    branch_predictor_t::periodic_check();

    #ifdef BRANCH_PREDICTOR_DEBUG
        this->print_structures();
    #endif
};

// ============================================================================
/// STATISTICS
// ============================================================================
void branch_predictor_static_taken_t::reset_statistics() {
    branch_predictor_t::reset_statistics();

    this->set_stat_btb_accesses(0);
    this->set_stat_btb_hit(0);
    this->set_stat_btb_miss(0);
};

// ============================================================================
void branch_predictor_static_taken_t::print_statistics() {
    branch_predictor_t::print_statistics();

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_btb_accesses", stat_btb_accesses);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_btb_hit", stat_btb_hit);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_btb_miss", stat_btb_miss);
    sinuca_engine.write_statistics_value_percentage(get_type_component_label(), get_label(), "stat_btb_miss_ratio", stat_btb_miss, stat_btb_hit + stat_btb_miss);
};

// ============================================================================
// ============================================================================
void branch_predictor_static_taken_t::print_configuration() {
    branch_predictor_t::print_configuration();

    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "btb_line_number", btb_line_number);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "btb_associativity", btb_associativity);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "btb_total_sets", btb_total_sets);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "btb_replacement_policy", get_enum_replacement_char(btb_replacement_policy));

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "btb_tag_bits_mask", utils_t::address_to_binary(btb_tag_bits_mask).c_str());
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "btb_index_bits_mask", utils_t::address_to_binary(btb_index_bits_mask).c_str());

};
