#!/bin/bash

npbdir="executions/NPB3.3-OMP/bin/"
tracegendir="trace_generator/extras/pinplay/bin/intel64/sinuca_tracer.so"

source source_pin.sh

for app in ft mg sp lu bt is ep cg ua; do
  for threads in 1 2 4 8; do
    export OMP_NUM_THREADS=${threads}

    # com parallel_start 1 e parallel_end 2 ele faz o esquema do arthur de indicar as regiões paralelas e o tempo delas
    pin -t ${tracegendir} -threads ${threads} -parallel_start 1 -parallel_end 2 -logfile ./executions/traces/ -- ${npbdir}/${app}.A.x
  done
done
