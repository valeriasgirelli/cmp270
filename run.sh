#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail -o posix

metrics=(cycles L1-dcache-loads L1-dcache-load-misses L1-dcache-stores LLC-loads LLC-load-misses LLC-stores offcore_requests.demand_rfo l2_rqsts.all_rfo l2_rqsts.rfo_miss l2_store_lock_rqsts.all l2_store_lock_rqsts.miss l2_trans.rfo offcore_requests_outstanding.demand_rfo offcore_response.demand_rfo.llc_hit.hitm_other_core offcore_response.demand_rfo.llc_miss.remote_hitm offcore_response.all_reads.llc_hit.any_response offcore_response.all_reads.llc_miss.any_response)

for iter in {1..30}; do
  for threads in 1 2 4 8; do
    for METRIC in "${metrics[@]}"; do
      for app in ft mg sp lu bt is ep cg ua; do
        sbatch --error="slurm/4thexec/%j_$app.A.err" --output="slurm/4thexec/%j_$app.A.out" -J "$app.A.$threads" run.slurm.msserpa $app $METRIC $threads
      done
    done
  done
done
