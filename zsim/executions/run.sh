#!/bin/bash

#cd slurm
#rm -f *
#cd ../

set -o errexit -o nounset -o pipefail -o posix

for threads in 1 2 4 8; do
  for app in ft mg sp lu bt is ep cg ua; do
    sbatch --error="slurm/%j_$app.A.err" --output="slurm/%j_$app.A.out" -J "zsim.$app.A.$threads" run.slurm $app $threads
  done
done
